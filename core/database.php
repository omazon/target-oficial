<?php
$db = new PDO( 'mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS );
$db->exec( 'set names utf8' );

class PostData{
	public static function form_into($data,$db){
		$smtp = $db->prepare( "insert into target_sitio (name,phone,email,message) values (?,?,?,?)");
		$smtp->bindParam( 1, $data->name );
		$smtp->bindParam( 2, $data->phone );
		$smtp->bindParam( 3, $data->email );
		$smtp->bindParam( 4, $data->message );
		if ( $smtp->execute() ) {
			require 'mail.php';
			send_mail($data,'mail_templates/formulario_principal.html');
			echo( json_encode( $response = array(
				'msg'    => 'Tus datos han sido enviado con éxito.',
				'status' => 1
			)));
		} else {
			echo( 'Ha ocurrido un error' );
		}
	}
}